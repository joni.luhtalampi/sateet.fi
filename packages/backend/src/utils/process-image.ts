import sharp from 'sharp';
import { Image } from '../models/image';

const formatImage = (image: sharp.Sharp): Image => {
  return { webp: image.webp(), png: image.png() };
};

const processImage = async (buffer: Buffer): Promise<Image> => {
  const { data, info } = await sharp(buffer)
    .ensureAlpha()
    .raw()
    .toBuffer({ resolveWithObject: true });
  for (let i = 0; i < data.length; i += 4) {
    // eslint-disable-next-line no-bitwise
    const color = (data[i] << 16) | (data[i + 1] << 8) | data[i + 2];
    if (color === 0xffffff || color === 0xf7f7f7) {
      // set gray and white to fully transparent
      data[i + 3] = 0;
    }
  }
  const { width, height } = info;
  const image = sharp(data, { raw: { width, height, channels: 4 } });
  return formatImage(image);
};

export default processImage;
