import { validationResult } from 'express-validator';
import { Request, Response, NextFunction } from 'express';

const validationHandler = (req: Request, res: Response, next: NextFunction): void => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(422).send({ errors: errors.array() });
  } else {
    next();
  }
};

export default validationHandler;
