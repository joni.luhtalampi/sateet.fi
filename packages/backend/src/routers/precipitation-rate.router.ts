import express from 'express';
import { check } from 'express-validator';
import moment from 'moment';
import validationHandler from '../utils/validation-hadler';
import precipitationRateService from '../services/precipitation-rate.service';

const validateTimestamp = (timestamp: string): boolean => {
  const time = moment(timestamp);
  if (moment().subtract(5, 'minutes').isSameOrBefore(time)) {
    throw new Error('Image for that timestamp is not yet available!');
  } else if (moment().subtract(7, 'days').isSameOrAfter(time)) {
    throw new Error('Images older than a week are not available!');
  } else if (time.valueOf() % 300000 !== 0) {
    throw new Error('The timestamp must be aligned to 5 minutes!');
  } else {
    return true;
  }
};

const router = express.Router();

router.get(
  '/:timestamp',
  check('timestamp')
    .isISO8601()
    .withMessage('The value is not in ISO 8601 format')
    .custom(validateTimestamp),
  validationHandler,
  async (req, res) => {
    try {
      const image = await precipitationRateService.getImage(req.params.timestamp);
      res.set('Cache-Control', 'public, max-age=86400');
      res.format({
        'image/webp': async () => {
          image.webp.pipe(res);
        },
        'image/png': async () => {
          image.png.pipe(res);
        },
      });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error);
      res.status(500).send({ error: 'Internal server error!' });
    }
  },
);

export default router;
