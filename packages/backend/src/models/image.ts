import sharp from 'sharp';

type ImageFormat = 'webp' | 'png';

export type Image = Record<ImageFormat, sharp.Sharp>;
