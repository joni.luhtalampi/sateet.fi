import express from 'express';
import path from 'path';
import cors from 'cors';
import compression from 'compression';
import helmet from 'helmet';
import precipitationRateRouter from './routers/precipitation-rate.router';

const app = express();
// https://expressjs.com/en/advanced/best-practice-security.html
// content security policy causes problems in heroku
app.use(helmet({ contentSecurityPolicy: false }));
app.use(cors());
app.use(compression());
app.use(express.static(path.join(__dirname, '../../frontend/build')));

app.use('/api/precipitation-rate', precipitationRateRouter);

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port ${PORT}`);
});
