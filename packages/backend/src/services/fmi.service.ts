import axios from 'axios';

const getPrecipitationRateImage = async (timestamp: string): Promise<Buffer> => {
  try {
    const url = 'https://openwms.fmi.fi/geoserver/wms';
    const params = {
      service: 'WMS',
      version: '1.3.0',
      request: 'GetMap',
      layers: 'Radar:suomi_rr_eureffin',
      time: timestamp,
      format: 'image/png',
      width: 800,
      height: 1200,
      // lat,lon,lat,lon: 57.99, 17.74, 69.34, 33.40
      bbox: '1974807.77,7965217.14,3718070.99,10857225.89',
      crs: 'EPSG:3857',
    };
    const res = await axios.get<ArrayBuffer>(url, { responseType: 'arraybuffer', params });
    return Buffer.from(res.data);
  } catch (error) {
    throw new Error('Cannot fetch precipitation rate image from FMI!');
  }
};

export default { getPrecipitationRateImage };
