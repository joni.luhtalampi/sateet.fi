import NodeCache from 'node-cache';
import moment from 'moment';
import { Image } from '../models/image';
import fmiService from './fmi.service';
import processImage from '../utils/process-image';

const imageCache = new NodeCache({ checkperiod: 3600 });

const updateCache = (image: Image, timestamp: string): void => {
  // Format both types at once as they both might be needed
  const ttl = moment(timestamp).diff(moment().subtract(7, 'days'), 'seconds');
  imageCache.set(timestamp, image, ttl);
};

const getImage = async (timestamp: string): Promise<Image> => {
  if (!imageCache.has(timestamp)) {
    const imageBuffer = await fmiService.getPrecipitationRateImage(timestamp);
    const image = await processImage(imageBuffer);
    updateCache(image, timestamp);
  }
  return imageCache.get<Image>(timestamp) as Image;
};

export default { getImage };
