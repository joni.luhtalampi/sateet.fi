import React from 'react';
import RainRadar from '../RainRadar';

const App: React.FC = () => {
  return <RainRadar />;
};

export default App;
