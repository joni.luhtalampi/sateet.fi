FROM node:16

WORKDIR /app

# install dependencies
COPY package.json yarn.lock ./
COPY packages/frontend/package.json ./packages/frontend/
COPY packages/backend/package.json ./packages/backend/
RUN yarn install