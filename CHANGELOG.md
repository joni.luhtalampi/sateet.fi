# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.1](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.3.0...v1.3.1) (2021-12-20)


### Bug Fixes

* removes UI file caching for now as is causing problems ([e42870a](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/e42870a9b1614658743affbb9439a87b813aaa5a))

## [1.3.0](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.2.2...v1.3.0) (2021-12-20)


### Features

* stores the map position to local storage ([c71d76b](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/c71d76b89d12361926e128f1ef984c9e14c8ecda))

### [1.2.2](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.2.1...v1.2.2) (2021-12-19)


### Bug Fixes

* fixes transpiling issue wit mapbox-gl ([b3fbe3a](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/b3fbe3a2146b9d114a5250a74cf7763e8367bb50))

### [1.2.1](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.2.0...v1.2.1) (2021-12-19)


### Bug Fixes

* fixes memorization issues with image preload ([7aad644](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/7aad64410cdaa3403113f7d940d8d1e11db76a72))
* fixes preloading issue with rain radar images ([9406404](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/9406404fafdb80410bbae8a273270d59e804f3c9))
* mapbox-gl loader problem ([2f18376](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/2f183765fe06de8b0a17d73eb1306075f719afdf))

## [1.2.0](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.1.5...v1.2.0) (2021-01-17)


### Features

* **UI:** radar images are now loaded in advance for better performance ([6936f2b](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/6936f2b572634dbd6afc660a5d18a9c5e80eef45)), closes [#26](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/26)


### Bug Fixes

* **UI:** app's title and descriptions were changed ([9810558](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/98105586cfbf316e940b44a112e8017b7715b765)), closes [#30](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/30)
* **UI:** favicon was created for the app ([82651cd](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/82651cddc7d6ed32c08faa1cca9451fbc7c242ac)), closes [#23](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/23)
* **UI:** logo & favicon were updated ([a00ccce](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/a00ccce324371dbceaaa2556ac5f94989e91d196)), closes [#23](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/23)

### [1.1.5](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.1.4...v1.1.5) (2021-01-13)


### Bug Fixes

* **UI:** Rain radar image's opacity was decreaced ([cc6dc46](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/cc6dc460c1d768761f0c04a87e4c8b69e19375d5)), closes [#24](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/24)
* **UI:** shorted timeout between rain radar images ([3e7078c](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/3e7078cac427b0f3070faf6d361ea4335c491d59)), closes [#25](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/25)
* **UI:** smoother transition between images, no flickering anymore ([3a83403](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/3a834037ec8d9cd6bc2a2773563c1318bfbd1715)), closes [#19](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/19)

### [1.1.4](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.1.3...v1.1.4) (2021-01-13)


### Bug Fixes

* **backend:** csp-headers causes problems in heroku ([295dcd5](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/295dcd5ca0871036eafaecc08cff2432175f2869))

### [1.1.3](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.1.2...v1.1.3) (2021-01-13)


### Bug Fixes

* **UI:** API was called twice for the same image ([ff4083a](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/ff4083a728f55c83769aff54d0d5586690f2a635)), closes [#20](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/20)

### [1.1.2](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.1.1...v1.1.2) (2021-01-09)


### Bug Fixes

* **frontend:** flex-gap problem with iPhone was properly fixed ([13d3726](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/13d372659a652a8dc91628bcab9584c91282c51e)), closes [#16](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/16)

### [1.1.1](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.1.0...v1.1.1) (2021-01-09)


### Bug Fixes

* **frontend:** Viewport and flex-gap issues with iPhone were fixed ([d43fd70](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/d43fd70e3d6a57d991ce24c9790e577babed2b27)), closes [#16](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/16)

## [1.1.0](https://gitlab.com/joni.luhtalampi/sateet.fi/compare/v1.0.0...v1.1.0) (2021-01-09)


### Features

* **frontend:** control panel was moved from bottom to right ([014213e](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/014213e073a03dfa2e441b4ca1de84b37ed546d1)), closes [#12](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/12)


### Bug Fixes

* **frontend:** Better styling for image navigation ([9ceba41](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/9ceba41a5f0390cb38d6edd58b65fb651445c927)), closes [#13](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/13)

## 1.0.0 (2021-01-07)


### Features

* **backend:** Cache-Control for precipitation rate images was added ([fa68d04](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/fa68d04255fb38d84d5273317d7100ada0a6970c)), closes [#8](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/8)
* **backend:** Endpoint for precipitation rates was added ([a651f39](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/a651f39709ddb1521c7c1f66e0ae2a907b3703b9)), closes [#3](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/3)
* **frontend:** 12h rain radar animation with animation controls was added ([8763205](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/8763205e91b29febb91945693879ba12c8fe88eb)), closes [#2](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/2) [#7](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/7)
* **frontend:** Basic map for rain radar was added ([8fce988](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/8fce988bc8652a28874b44d93faacd47c02b74e2)), closes [#1](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/1)
* **frontend:** rain radar edge was added ([b9a61b4](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/b9a61b41f7eecaf412a3b1a931df656d4b5d6bf3)), closes [#4](https://gitlab.com/joni.luhtalampi/sateet.fi/issues/4)


### Bug Fixes

* **backend:** radar image coordinates were slightly adjusted ([052a209](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/052a2098caca449dcc2446f409cfd4c18fd6abc7))
* **backend/precipitation-rate:** better validation for timestamp was added ([6535bfc](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/6535bfc6684834157b82b8a511bba2e46f34c5ff))
* **frontend:** added workaround for mapbox-gl v.2.0.1 reference error ([39b02a0](https://gitlab.com/joni.luhtalampi/sateet.fi/commit/39b02a0c55d720ec722d4e8985c37e4565ca42f4))
