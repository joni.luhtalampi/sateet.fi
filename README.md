# Sateet.fi

## Getting started

1. Install [nodenv](https://github.com/nodenv/nodenv).

   ```console
   foo@bar:~$ poetry install nodenv
   ```

1. Use nodenv to install project [Node.js](https://nodejs.org/en/) version.

   ```console
   foo@bar:~$ nodenv install
   ```

1. Install yarn. Project takes the advantages of [yarn workspaces](https://classic.yarnpkg.com/lang/en/docs/workspaces/).

   ```console
   foo@bar:~$ npm install -g yarn
   ```

1. Install dependencies

   ```console
   foo@bar:~$ yarn install
   ```

### Build application

```console
foo@bar:~$ yarn build
```

### Run application in production mode

```console
foo@bar:~$ yarn build && yarn start
```

### Run application in development mode

```console
foo@bar:~$ yarn dev
```

### Containerized development

```console
foo@bar:~$ npm run docker:dev
```
